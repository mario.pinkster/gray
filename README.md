# gray
This simple example project demonstrates an issue that we are currently experiencing
while upgrading our application from Hippo version 12.6  to version 13.3, and a solution.

In our application we have defined a multitude of ValueListProviders with many dependencies.
The ValueListProviders are defined in the site project but they are used in the CMS project.

Some dependencies of the ValueListProviders (notably on PageComposerContextService) are broken 
by version 13 of Hippo because the relevant libraries are no longer included in the site project, 
and they cannot be added back without breaking other basic things that happen deep inside Bloomreach
provided code.

Moving all of our ValueListProviders from the site project to a common library would be one possible
solution to this problem, but this would mean a major refactoring of our code base (relocating several
hundreds of source files). Understandably, our customer is not pleased that we would embark on 
such a large scale adventure without any tangible result showing at the end (other than upgrading
the Hippo version from 12 to 13).

The text file "problem-description.txt" explains with somewhat more verbosity what exactly is the
problem.

# branches
The **master** branch has all the code necessary for running the project with Hippo 12.6
The **demo-hippo-13** branch has code that provides the same function running in Hip[po 13.3

