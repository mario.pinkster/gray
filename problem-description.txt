Author: Mario Pinkster (mario.pinkster@indivirtual.com)
project url https://gitlab.com/mario.pinkster/gray

This example project attempts to illustrate a problem we are having with ValueListProviders 
while upgrading from version 12.6.1-1 to 13.3.0

The git repository has a working barebone project that uses a ValueListProvider to drive a 
dropdown widget in the channel manager.
This project can be built and run from the master branch:

mvn clean package
mvn -Pcargo.run

To see the dropdown:

Go to localhost:8080/cms
Login as admin with password admin.
Open the (only) channel in the channel manager.
On the home page of that channel there is one instance of the TextRepeater component.
Open the editor for this component.
The dropdown in question is used for selecting the color of the title bar.

To see where the valuelist for this dropdown is defined and how it is connected:

In content/documents/system two value lists are defined: colors-hard and soft-colors.
In the settings for the channel, you can set a property that holds the path to one of
these two value lists (it is the only property in the channel settings, named bgColorList).
When you change the name there to the name of the other value list, the dropdown in 
the editor for the Text Repeater component will display another list of colors.

All this works as expected with version 12.6.1-1 of Hippo.
However it is broken in version 13.3.0.

The other branch in the git repository, named development, illustrates what our problem is:

Following the upgrade instructions  provided by the Bloomreach documentation website,
I have initially removed the dependency for hst-page-composer from the site project.
This results in a broken compilation for the ColorValueListProvider in the site project.

Experimentally adding back this dependency for version 13.3.0 results in another missing 
dependency, this time for org/hippoecm/hst/platform/api/PlatformServices.

Experimentally adding back the dependency for hst-platform to the site project results in
a project that compiles and appears to start up.

However, the site application does not start up properly because of this error:

    Invocation of init method failed; nested exception is java.lang.NullPointerException

which has this underlying cause:
   
    A service of type org.hippoecm.hst.container.site.CustomWebsiteHstSiteProviderService is already registered.

Clearly adding back the removed dependencies is not the solution to our problem.

Another approach that I have tried (in our real project) is moving all ValueListProviders to a common custom 
library of things shared between the site and the CMS. While this could work in theory,
the reality is that there are a multitide of ValueListProviders and between them they
have a large amount of other dependencies which must also be moved, in turn causing still other
dependencies to be moved as well. 

This is a major refactoring operation and a lot of work, not only for developers but
especially for testers who have to ascertain that everything still works afterwards. 

And understandably, our customer is not pleased that this overhaul will cost a lot of time and money
while yielding zero visible results.


So our question is this: how do we fix the broken ValueListProviders while upgrading from 12.6.1-1 to 13.3.0
without having to spend a disproportionate amount of time and resources in doing so?

