package nl.dimario.beans;

import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoDocument;

@Node(jcrType="gray:basedocument")
public class BaseDocument extends HippoDocument {

}
