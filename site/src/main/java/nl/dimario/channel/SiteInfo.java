package nl.dimario.channel;

import org.hippoecm.hst.configuration.channel.ChannelInfo;
import org.hippoecm.hst.core.parameters.Parameter;

public interface SiteInfo extends ChannelInfo {

    @Parameter(name="bgColorList", displayName = "List of background colors")
    String getBgColorListPath();
}
