package nl.dimario.site.components;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.hippoecm.hst.configuration.hosting.Mount;
import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.content.beans.ObjectBeanManagerException;
import org.hippoecm.hst.content.beans.manager.ObjectBeanManager;
import org.hippoecm.hst.core.container.ComponentManager;
import org.hippoecm.hst.core.parameters.ValueListProvider;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.hippoecm.hst.core.request.ResolvedMount;
import org.hippoecm.hst.pagecomposer.jaxrs.services.PageComposerContextService;
import org.hippoecm.hst.site.HstServices;
import org.onehippo.forge.selection.hst.contentbean.ValueList;
import org.onehippo.forge.selection.hst.contentbean.ValueListItem;

import nl.dimario.channel.SiteInfo;

public class ColorValueListProvider implements ValueListProvider {

    private  ValueList colorList;

    private ValueList getValueList(String path) { HstRequestContext hstRequestContext = RequestContextProvider.get();
        if( hstRequestContext == null) {
            throw new RuntimeException("Cannot obtain hstRequestContext");
        }
        ObjectBeanManager objectBeanManager = hstRequestContext.getObjectBeanManager();
        try {
            Object objectBean = objectBeanManager.getObject(path);
            return (ValueList)objectBean;
        } catch (ObjectBeanManagerException | ClassCastException e) {
            throw new RuntimeException( "Failed to find hippo bean by path: " + path);
        }
    }

    private void init() {
        if( colorList != null) {
            return;
        }
        ComponentManager componentManager = HstServices.getComponentManager();
        PageComposerContextService pageComposerContextService = componentManager.getComponent(PageComposerContextService.class, "org.hippoecm.hst.pagecomposer");
        Mount mount = pageComposerContextService.getEditingMount();
        SiteInfo siteInfo = mount.getChannelInfo();
        String valueListPath = siteInfo.getBgColorListPath();

        // This demonstrates that the solution proposed by  Bloomreach does
        // NOT work in version 12 and also not in version 13.
        HstRequestContext hrc = RequestContextProvider.get();
        ResolvedMount rm = hrc.getResolvedMount();
        mount = rm.getMount();

        // The returned mount does not have a channel associated with it and no channelinfo.
        Object o = mount.getChannel();   // is null
        siteInfo = mount.getChannelInfo();   // is null

        colorList = getValueList( valueListPath);
    }

    private String findLabel( String key) {
        if (colorList != null) {
            return colorList.getItems().stream().filter(valueListItem -> StringUtils.equals(valueListItem.getKey(), key)).findFirst()
                .map(ValueListItem::getLabel).orElse(StringUtils.EMPTY);
        }
        return StringUtils.EMPTY;
    }

    @Override
    public List<String> getValues() {
        init();
        if( colorList != null) {
            return colorList.getItems().stream().map(ValueListItem::getKey).collect(Collectors.toList());
        }
        // if no color list found, return empty list.
        return new ArrayList<String>();
    }

    @Override
    public String getDisplayValue(String value) {
        return getDisplayValue( value,  null);
    }

    @Override
    public String getDisplayValue(String value, Locale locale) {
        final String displayValue = findLabel(value);
        return (displayValue != null) ? displayValue : value;
    }
}
