package nl.dimario.site.components;

import java.util.ArrayList;
import java.util.List;

import org.hippoecm.hst.component.support.bean.BaseHstComponent;
import org.hippoecm.hst.core.component.HstComponentException;
import org.hippoecm.hst.core.component.HstRequest;
import org.hippoecm.hst.core.component.HstResponse;
import org.hippoecm.hst.core.parameters.ParametersInfo;

@ParametersInfo(type = TextRepeaterComponentInfo.class)
public class TextRepeaterComponent extends BaseHstComponent {

    @Override
    public void doBeforeRender(HstRequest request,
                               HstResponse response) throws HstComponentException {
        super.doBeforeRender(request, response);
        TextRepeaterComponentInfo info = getComponentParametersInfo(request);

        String repeatText = info.getRepeatText();
        int repeatCount = info.getRepeatCount();
        String bgColor = info.getBgColor();
        request.setAttribute("bgColor", bgColor);

        // Prepare repeated text in list
        List<String> list = new ArrayList<String>( repeatCount);
        for( int i=0; i<repeatCount; i++) {
            String text = String.format( "%03d: %s", i, repeatText);
            list.add( text);
        }
        request.setAttribute( "repeatList", list);
    }
}
