package nl.dimario.site.components;

import org.hippoecm.hst.core.parameters.DropDownList;
import org.hippoecm.hst.core.parameters.Parameter;

public interface TextRepeaterComponentInfo {

    @Parameter(name = "repeatCount",
            defaultValue = "7",
            displayName = "Repeat count")
    int getRepeatCount();

    @Parameter(name = "repeatText",
            defaultValue = "This message is repeated",
            displayName = "Text to repeat")
    String getRepeatText();

    @Parameter(name="bgColor", displayName = "Color of title bar")
    @DropDownList(valueListProvider = ColorValueListProvider.class)
    String getBgColor();
}
